import Vue from 'vue'
import Router from 'vue-router'
import Home from './routes/Home.vue'
import Interaction from './routes/Interaction.vue'
import Ticket from './routes/Ticket.vue'
import Family from './routes/Family.vue'
import Contact from './routes/Contact.vue'
import Help from './routes/Help.vue'
import Search from './routes/Search.vue'
import Settings from './routes/Settings.vue'
import Totals from './routes/Totals.vue'

Vue.use(Router)

///////////////////////////////////


var array = [];
// [
//     {
//       path: '/',
//       name: 'home',
//       component: Home
//     },
//     {
//       path: '/about',
//       name: 'about',
//       // route level code-splitting
//       // this generates a separate chunk (about.[hash].js) for this route
//       // which is lazy-loaded when the route is visited.
//       component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
//     }
//   ]

///////////////////////////////////

array.push({
  name:'home',
  path:'/',
  title:'Home',
  component:Home,
})

array.push({
  name:'search',
  path:'/search',
  title:'Search',
  component:Search,
})

// array.push({
//   name:'help',
//   path:'/help',
//   title:'Help',
//   component:Help,
// })


array.push({
  name:'settings',
  path:'/settings',
  title:'Settings',
  component:Settings,
})

array.push({
  name:'totals',
  path:'/totals',
  title:'Totals',
  component:Totals,
})


array.push({
  name:'interaction',
  path:'/interaction/:id',
  title:'Interaction Tickets',
  component:Interaction,
})


array.push({
  name:'ticket',
  path:'/ticket/:id',
  title:'Ticket',
  component:Ticket,
})

array.push({
  name:'contact',
  path:'/contact/:id',
  title:'Contact',
  component:Contact,
})
array.push({
  name:'family',
  path:'/family/:id',
  title:'Family',
  component:Family,
})


///////////////////////////////////

export default new Router({
	 mode: 'history',
  routes: array,
})

///////////////////////////////////

